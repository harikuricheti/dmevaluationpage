﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AutoCompleteDemo.Models
{
    public class CompanyNames
    {
        public string CompanyName { get; set; }
        public string SalesForceAccountid { get; set; }
        public string ModifiedCompanyName { get; set; }

        public static List<CompanyNames> GetAllCompanyNames()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SynchubConStr"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "MIG_Get_All_CompanyNames";
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;


            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            List<CompanyNames> CompanyList = new List<CompanyNames>();
            while (dr.Read())
            {
                CompanyNames Comp = new CompanyNames();

                Comp.CompanyName = dr["CompanyName"].ToString();
                Comp.ModifiedCompanyName = dr["ModifiedCompanyName"].ToString();
                Comp.SalesForceAccountid = dr["SalesForceAccountId"].ToString();

                CompanyList.Add(Comp);

            }

            return CompanyList;
        }

        public static List<CompanyNames> GetAllCompanyNames(string strSearchvalue)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SynchubConStr"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "MIG_Get_All_CompanyNames_WithSearch";
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SearchString", strSearchvalue);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            List<CompanyNames> CompanyList = new List<CompanyNames>();
            while (dr.Read())
            {
                CompanyNames Comp = new CompanyNames();

                Comp.CompanyName = dr["CompanyName"].ToString();
                Comp.ModifiedCompanyName = dr["ModifiedCompanyName"].ToString();
                Comp.SalesForceAccountid = dr["SalesForceAccountId"].ToString();

                CompanyList.Add(Comp);

            }

            return CompanyList;
        }
    }

    
}