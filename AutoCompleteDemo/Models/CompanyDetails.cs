﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AutoCompleteDemo.Models
{
    public class CompanyDetails
    {




        public string AtlasCompanyID { get; set; }
        public string SalesForceAccountID { get; set; }
        public string CitWebID { get; set; }
        public string CompanyName { get; set; }
        public string CitWebParentID { get; set; }
        public string CitwebParentCompanyName { get; set; }
        public string Segmentation { get; set; }
        public string NextContractExpirydate { get; set; }
        public string ServicesTaken { get; set; }
        public string CitwebEmployees { get; set; }
        public string AtlasEmployees { get; set; }
        public string Employeesmatching { get; set; }
        public string CitwebBankEmployees { get; set; }
        public string CitwebUsers { get; set; }
        public string CitwebRiskAssessments { get; set; }
        public string LIVERAHavingAdditionalICons { get; set; }
        public string AtlasRiskAssessments { get; set; }
        public string CitwebLastlogin { get; set; }
        public string AtlasLastlogin { get; set; }
        public string Lastholidayrequested { get; set; }
        public string Atlasmigrationalready { get; set; }
        public string Numberofsites { get; set; }
        public string ProjectPhaseandComplexity { get; set; }
        public string CriticalExceptions { get; set; }
        public string CriticalExceptionNumbers { get; set; }
        public string SchedulingstatusinSalesforce { get; set; }
        public string SchedulingdateinSalesforce { get; set; }
        public string CitwebSuperUserName { get; set; }
        public string ShorthornGeneralContactName { get; set; }
        public string GeneralContactEmailaddress { get; set; }
        public string MyOfficeClientId { get; set; }
        public string MyOfficeClientActive { get; set; }
        public string ServiceOwnerEmail { get; set; }
        public string ReasonForNo { get; set; }
        public string ReadyForMigration { get; set; }

        public List<SiteExceptions> SiteExceptionsList { get; set; }


        public static CompanyDetails GetCompanyDetails(string SFDCID)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SynchubConStr"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            DataSet dsCompanyData = new DataSet();
            

            cmd.CommandText = "MIG_Get_CompanyDetails";
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@SFDCID", SFDCID);

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dsCompanyData);


            //if (con.State != ConnectionState.Open)
            //{
            //    con.Open();
            //}
            //SqlDataReader dr = cmd.ExecuteReader();
            //dr.Read();
            CompanyDetails CompanyInfo = new CompanyDetails();

            if (dsCompanyData.Tables.Count > 0)
                if (dsCompanyData.Tables[0].Rows.Count > 0)
                {

                    CompanyInfo.AtlasCompanyID = dsCompanyData.Tables[0].Rows[0]["AtlasCompanyID"].ToString();
                    CompanyInfo.SalesForceAccountID = dsCompanyData.Tables[0].Rows[0]["SalesForceAccountID"].ToString();
                    CompanyInfo.CitWebID = dsCompanyData.Tables[0].Rows[0]["CitWebID"].ToString();
                    CompanyInfo.CompanyName = dsCompanyData.Tables[0].Rows[0]["CompanyName"].ToString();
                    CompanyInfo.CitWebParentID = dsCompanyData.Tables[0].Rows[0]["CitWebParentID"].ToString();
                    CompanyInfo.CitwebParentCompanyName = dsCompanyData.Tables[0].Rows[0]["CitwebParentCompanyName"].ToString();
                    CompanyInfo.Segmentation = dsCompanyData.Tables[0].Rows[0]["Segmentation"].ToString();
                    CompanyInfo.NextContractExpirydate = dsCompanyData.Tables[0].Rows[0]["NextContractExpirydate"].ToString();
                    CompanyInfo.ServicesTaken = dsCompanyData.Tables[0].Rows[0]["ServicesTaken"].ToString();
                    CompanyInfo.CitwebEmployees = dsCompanyData.Tables[0].Rows[0]["CitwebEmployees"].ToString();
                    CompanyInfo.AtlasEmployees = dsCompanyData.Tables[0].Rows[0]["AtlasEmployees"].ToString();
                    CompanyInfo.Employeesmatching = dsCompanyData.Tables[0].Rows[0]["Employeesmatching"].ToString();
                    CompanyInfo.CitwebBankEmployees = dsCompanyData.Tables[0].Rows[0]["CitwebBankEmployees"].ToString();
                    CompanyInfo.CitwebUsers = dsCompanyData.Tables[0].Rows[0]["CitwebUsers"].ToString();
                    CompanyInfo.CitwebRiskAssessments = dsCompanyData.Tables[0].Rows[0]["CitwebRiskAssessments"].ToString();
                    CompanyInfo.AtlasRiskAssessments = dsCompanyData.Tables[0].Rows[0]["AtlasRiskAssessments"].ToString();
                    CompanyInfo.LIVERAHavingAdditionalICons = dsCompanyData.Tables[0].Rows[0]["LIVERAHavingAdditionalICons"].ToString();
                    CompanyInfo.CitwebLastlogin = dsCompanyData.Tables[0].Rows[0]["CitwebLastlogin"].ToString();
                    CompanyInfo.AtlasLastlogin = dsCompanyData.Tables[0].Rows[0]["AtlasLastlogin"].ToString();
                    CompanyInfo.Lastholidayrequested = dsCompanyData.Tables[0].Rows[0]["Lastholidayrequested"].ToString();
                    CompanyInfo.Atlasmigrationalready = dsCompanyData.Tables[0].Rows[0]["Atlasmigrationalready"].ToString();
                    CompanyInfo.Numberofsites = dsCompanyData.Tables[0].Rows[0]["Numberofsites"].ToString();
                    CompanyInfo.ProjectPhaseandComplexity = dsCompanyData.Tables[0].Rows[0]["ProjectPhaseandComplexity"].ToString();
                    CompanyInfo.CriticalExceptions = dsCompanyData.Tables[0].Rows[0]["CriticalExceptions"].ToString();
                    CompanyInfo.CriticalExceptionNumbers = dsCompanyData.Tables[0].Rows[0]["CriticalExceptionNumbers"].ToString();
                    CompanyInfo.SchedulingstatusinSalesforce = dsCompanyData.Tables[0].Rows[0]["SchedulingstatusinSalesforce"].ToString();
                    CompanyInfo.SchedulingdateinSalesforce = dsCompanyData.Tables[0].Rows[0]["SchedulingdateinSalesforce"].ToString();
                    CompanyInfo.CitwebSuperUserName = dsCompanyData.Tables[0].Rows[0]["CitwebSuperUserName"].ToString();
                    CompanyInfo.ShorthornGeneralContactName = dsCompanyData.Tables[0].Rows[0]["ShorthornGeneralContactName"].ToString();
                    CompanyInfo.GeneralContactEmailaddress = dsCompanyData.Tables[0].Rows[0]["GeneralContactEmailaddress"].ToString();
                    CompanyInfo.MyOfficeClientId = dsCompanyData.Tables[0].Rows[0]["MyOfficeClientId"].ToString();
                    CompanyInfo.MyOfficeClientActive = dsCompanyData.Tables[0].Rows[0]["MyOfficeClientActive"].ToString();
                    CompanyInfo.ServiceOwnerEmail = dsCompanyData.Tables[0].Rows[0]["ServiceOwnerEmail"].ToString();
                    CompanyInfo.ReasonForNo = dsCompanyData.Tables[0].Rows[0]["ReasonForNo"].ToString();
                    CompanyInfo.ReadyForMigration = dsCompanyData.Tables[0].Rows[0]["ReadyForMigration"].ToString();

                }
            if (dsCompanyData.Tables.Count > 1 && dsCompanyData.Tables[1].Rows.Count > 0)
            {

                List<SiteExceptions> lstSiteException = new List<SiteExceptions>();

                SiteExceptions objSiteExcepion = null;



                foreach (DataRow dr in dsCompanyData.Tables[1].Rows)
                {
                    objSiteExcepion = new SiteExceptions();

                    objSiteExcepion.CompanyID = dr["CompanyID"].ToString();
                    objSiteExcepion.CompanyName = dr["CompanyName"].ToString();
                    objSiteExcepion.SiteID = dr["SiteID"].ToString();
                    objSiteExcepion.SiteName = dr["SiteName"].ToString();
                    objSiteExcepion.SitePostCode = dr["SitePostCode"].ToString();
                    objSiteExcepion.ExceptionName = dr["ExceptionName"].ToString();
                    lstSiteException.Add(objSiteExcepion);
                }

                CompanyInfo.SiteExceptionsList = lstSiteException;
            }

                return CompanyInfo;


        }

    }
}