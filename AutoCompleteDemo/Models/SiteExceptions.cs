﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoCompleteDemo.Models
{
    public class SiteExceptions
    {

        public string CompanyID { get; set; }
        public string SiteID { get; set; }
        public string CompanyName { get; set; }
        public string SiteName { get; set; }
        public string SitePostCode { get; set; }
        public string ExceptionName { get; set; }

       
    }
}