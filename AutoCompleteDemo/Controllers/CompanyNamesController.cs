﻿using AutoCompleteDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AutoCompleteDemo.Controllers
{
    public class CompanyNamesController : ApiController
    {

        [HttpGet]
        public HttpResponseMessage GetAllCompanyNames(string term)
        {

            //return EmployeeModel.GetAllEmployees().Where(x=> x.EmpName.StartsWith(Name)).ToList();
            var datatest = CompanyNames.GetAllCompanyNames().Select(x => new { label = x.CompanyName.ToUpper(), value = x.SalesForceAccountid });
            var dataTest22 = datatest.Where(x => x.label.StartsWith(term.ToUpper().ToString()));
            var AVL = Request.CreateResponse(dataTest22);
            return AVL;



        }


        public HttpResponseMessage GetCompanyDetails(string SFDCID)
        {

            //return EmployeeModel.GetAllEmployees().Where(x=> x.EmpName.StartsWith(Name)).ToList();
            var datatest = CompanyDetails.GetCompanyDetails(SFDCID);
            
            var AVL = Request.CreateResponse(datatest);
            return AVL;



        }
    }
}
